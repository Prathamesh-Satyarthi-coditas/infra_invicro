terraform {
  required_version = ">= 0.15.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.29.0"
    }
  }
}
provider "aws" {
  region              = "ap-south-1"
  allowed_account_ids = [240633844458]
  default_tags {
    tags = {
      environment = "capstone-prod"
      managedby   = "terraform"
    }
  }
}