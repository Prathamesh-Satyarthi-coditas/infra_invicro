env = "dev"
vpc_conf = {
  cidr_block           = "10.10.0.0/24"
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true
}
# Public Subnets
public_subnet = {
  public-subnet-1 = {
    cidr_block        = "10.10.1.0/24"
    availability_zone = "ap-south-1a"
  }
  public-subnet-2 = {
    cidr_block        = "10.10.2.0/24"
    availability_zone = "ap-south-1b"
  }
}

# Appliction Subnets
application_subnet = {
  application-subnet-1 = {
    cidr_block        = "10.10.3.0/24"
    availability_zone = "ap-south-1a"
  }
  application-subnet-2 = {
    cidr_block        = "10.10.4.0/24"
    availability_zone = "ap-south-1b"
  }
}


# Public NACL Ingress Rules
public_nacl_ingress_rules = [
  {
    rule_no    = 100
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  },
  {
    rule_no    = 101
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  },
  {
    rule_no    = 103
    cidr_block = "10.10.0.0/16"
    from_port  = 22
    to_port    = 22
  },
  {
    rule_no    = 104
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  },
  {
    protocol   = "icmp"
    rule_no    = 105
    cidr_block = "0.0.0.0/0"
    icmp_type  = -1
    icmp_code  = -1
    from_port  = 0
    to_port    = 0
  }
]

# Public NACL Egress Rules
public_nacl_egress_rules = [
  {
    rule_no    = 100
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  },
  {
    rule_no    = 101
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  },
  {
    rule_no    = 104
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  },
  {
    protocol   = "icmp"
    rule_no    = 105
    cidr_block = "0.0.0.0/0"
    icmp_type  = -1
    icmp_code  = -1
    from_port  = 0
    to_port    = 0
  }
]

# Appliction NACL Ingress Rules
application_nacl_ingress_rules = [
  {
    rule_no    = 100
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  },
  {
    rule_no    = 101
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  },
  {
    rule_no    = 102
    cidr_block = "10.10.0.0/16"
    from_port  = 22
    to_port    = 22
  },
  {
    rule_no    = 103
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  },
  {
    rule_no    = 104
    cidr_block = "0.0.0.0/0"
    from_port  = 3000
    to_port    = 3000
  },
  {
    rule_no    = 105
    cidr_block = "0.0.0.0/0"
    from_port  = 3001
    to_port    = 3001
  },
  {
    rule_no    = 106
    cidr_block = "0.0.0.0/0"
    from_port  = 3002
    to_port    = 3002
  },
  {
    rule_no    = 107
    cidr_block = "0.0.0.0/0"
    from_port  = 3003
    to_port    = 3003
  },
  {
    rule_no    = 108
    cidr_block = "0.0.0.0/0"
    from_port  = 3004
    to_port    = 3004
  },
  {
    rule_no    = 109
    cidr_block = "0.0.0.0/0"
    from_port  = 3005
    to_port    = 3005
  },
  {
    rule_no    = 110
    cidr_block = "0.0.0.0/0"
    from_port  = 3006
    to_port    = 3006
  },
  {
    rule_no    = 111
    cidr_block = "0.0.0.0/0"
    from_port  = 5432
    to_port    = 5432
  },
  {
    rule_no    = 112
    cidr_block = "0.0.0.0/0"
    from_port  = 8000
    to_port    = 8000
  }
]

# Appliction NACL Egress Rules
application_nacl_egress_rules = [
  {
    rule_no    = 100
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  },
  {
    rule_no    = 101
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  },
  {
    rule_no    = 102
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  },
  {
    rule_no    = 103
    cidr_block = "0.0.0.0/0"
    from_port  = 25
    to_port    = 25
  },
  {
    rule_no    = 104
    cidr_block = "0.0.0.0/0"
    from_port  = 3000
    to_port    = 3000
  },
  {
    rule_no    = 105
    cidr_block = "0.0.0.0/0"
    from_port  = 3001
    to_port    = 3001
  },
  {
    rule_no    = 106
    cidr_block = "0.0.0.0/0"
    from_port  = 3002
    to_port    = 3002
  },
  {
    rule_no    = 107
    cidr_block = "0.0.0.0/0"
    from_port  = 3003
    to_port    = 3003
  },
  {
    rule_no    = 108
    cidr_block = "0.0.0.0/0"
    from_port  = 3004
    to_port    = 3004
  },
  {
    rule_no    = 109
    cidr_block = "0.0.0.0/0"
    from_port  = 3005
    to_port    = 3005
  },
  {
    rule_no    = 110
    cidr_block = "0.0.0.0/0"
    from_port  = 3006
    to_port    = 3006
  },
  {
    rule_no    = 111
    cidr_block = "0.0.0.0/0"
    from_port  = 5432
    to_port    = 5432
  },
  {
    rule_no    = 112
    cidr_block = "0.0.0.0/0"
    from_port  = 8000
    to_port    = 8000
  }
]
