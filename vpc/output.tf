output "vpc_id" {
  description = "vpc id"
  value       = aws_vpc.main.id
}
output "application_subnet-1" {
  description = "id of the application subnet 1"
  value       = aws_subnet.application_subnet["application-subnet-1"].id
}
output "application_subnet-2" {
  description = "id of the application subnet 2"
  value       = aws_subnet.application_subnet["application-subnet-2"].id
}
output "public_subnet-1" {
  description = "id of the public subnet 1"
  value       = aws_subnet.public_subnet["public-subnet-1"].id
}
output "public_subnet-2" {
  description = "id of the public subnet 2"
  value       = aws_subnet.public_subnet["public-subnet-2"].id
}
